# Theses are documents
*****
I've made these documents specfiically how I want to, which is, for now, in LaTex. It's up to the viewer to determine how they wish to edit or compile these documents.

## Changes
*****
If you have any requests for changes feel free to submit a pull request via the web interface or in any way you so choose. Marked up documents printed out may be put on my desk. Emails with changes will have the lowest priority.

I have no feelings when it comes to writing, no edits will be taken personally, the document has it's own life and any criticism will be taken as criticism of the document not the author, so do your worst.
